# 42-C-wolf3d

Ihis prgm is a game project based on a raycasting technic.
It was used in old games like Wolfenstein, Doom , Duke Nukem, quake etc...

#environment 
Mac os x el capitan

C langage

mini lib x

# Run
git clone https://github.com/rim31/42-C-wolf3d.git

cd 42-C-wolf3d

make

./wolf3d 1

(choose a map between 1 to 4)

![![Texte alternatif](https://github.com/rim31/42-C-wolf3d/blob/master/wolf.png "make")](https://bitbucket.org/repo/eLKRoK/images/1865127897-wolf.png)


* example map

```
#!C
1111111111111111111111111
1000000000000000000000001
1000000000000000000000001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000000000000000000000001
1000000000000000000000001
1000000000000000000000001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000111111100011111110001
1000000000000000000000001
1000000000000000000000001
1111111111111111111111111

```

# basic render :
move withe arrows : multi touch

move with mouse and left clic mouse

stop by walls

wall in differents colors accordint to directions

Esc to quit the prgm

![![Texte alternatif](https://github.com/rim31/42-C-wolf3d/blob/master/wolf2.png "bonus")](https://bitbucket.org/repo/eLKRoK/images/2473471664-wolf1.png)

# Bonus :
mini map : press 's'

see the map, you position in blue

the enemy in green : wihte colum (initially a blue shell)

mod "toad kart"

skybox

![![
![Texte alternatif](https://github.com/rim31/42-C-wolf3d/blob/master/wolf1.png "basic")](https://bitbucket.org/repo/eLKRoK/images/1740595678-wolf1.png)
](https://bitbucket.org/repo/eLKRoK/images/1462467490-wolf2.png)

# Ultimate bonus:
texture on walls press 'a' (in qwerty) or 'q' (in azerty)

'spacebar' to accelerate
![work_3.jpg](https://bitbucket.org/repo/eLKRoK/images/753022234-work_3.jpg)


# Difficulties : 
put textures.
It was fun to creat a game like wolfeinstein. I think out of the box, that why i didin't creat a shooting game but a mario kart like battle game. I wanted to create a rocket league & mario kart :)